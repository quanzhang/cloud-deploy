FROM zhangquangg/google-cloud-auth:1.7.1-ubuntu as auth-base

# FROM golang:1.19-alpine as builder
# WORKDIR /app
# COPY . .
# RUN go build -o cloud-deploy

# # Start a new stage to create a smaller image
# FROM alpine:latest
# RUN apk update && apk add bash curl
# WORKDIR /app
# COPY --from=builder /app/cloud-deploy /usr/bin/cloud-deploy
# COPY --from=auth-base /usr/bin/google-cloud-auth /usr/bin/google-cloud-auth
# CMD ["cloud-deploy"]

# Use the official Golang image as the builder stage
FROM golang:1.19 as builder
WORKDIR /app
COPY . .
RUN go build -o cloud-deploy

# Start a new stage with Ubuntu as the base image
FROM ubuntu:latest
RUN apt-get update && \
    apt-get install -y bash curl && \
    rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY --from=builder /app/cloud-deploy /usr/bin/cloud-deploy
COPY --from=auth-base /usr/bin/google-cloud-auth /usr/bin/google-cloud-auth
CMD ["cloud-deploy"]
