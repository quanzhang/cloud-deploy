package cmd

import (
	"cloud-deploy/pkg/auth"
	"fmt"

	"github.com/spf13/cobra"
)

var (
	wif       string
	oidc      string
	sa        string
	projectId string
	ciJobId   string
)

// authCmd represents the auth command
var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Create the credential file for cloud client library authentication using WIF and ADC",
	Long: `Credential files is created at: /tmp/oidc-credential-{ci-job-id}.json.
	See ADC details in: https://cloud.google.com/docs/authentication/application-default-credentials#GAC.
	See WIF in https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/`,
	RunE: func(cmd *cobra.Command, args []string) error {
		// oidc is the overall oidc object provided by gitlab
		// wif is the audience of oide

		// read jwt token input, write it to a JWT file
		jwtFile, err := auth.CreateJWTFile(ciJobId, oidc)
		if err != nil {
			return err
		}

		// compose the credential file, inlcuding JWT file
		credentialFile, err := auth.CreateCredentialFile(ciJobId, jwtFile, wif, sa)
		if err != nil {
			return err
		}

		fmt.Printf("auth completed, file: %s", credentialFile)

		// set env variable GOOGLE_APPLICATION_CREDENTIALS
		// if err = os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", credentialFile); err != nil {
		// 	return err
		// }

		return nil
	},
}

func init() {
	rootCmd.AddCommand(authCmd)

	authCmd.PersistentFlags().StringVarP(&projectId, "project-id", "p", "", "GCP Project Id")
	authCmd.PersistentFlags().StringVarP(&sa, "service-account", "s", "", "GCP Service Account")
	authCmd.PersistentFlags().StringVarP(&oidc, "oidc", "o", "", "OIDC")
	authCmd.PersistentFlags().StringVarP(&ciJobId, "ci-job-id", "", "", "Gitlab CI Job ID")
	authCmd.PersistentFlags().StringVarP(&wif, "wif", "", "", "Workload Identity Federation")
}
