package cmd

import (
	"cloud-deploy/pkg/release"
	"cloud-deploy/pkg/rollout"
	"context"
	"fmt"
	"os"

	deploy "cloud.google.com/go/deploy/apiv1"
	"cloud.google.com/go/storage"
	"github.com/spf13/cobra"
	"google.golang.org/api/option"
)

var (
	releaseName      string
	deliveryPipeline string
	source           string
	region           string
	credentialsFile  string
)

// releaseCmd represents the auth command
var releaseCmd = &cobra.Command{
	Use:   "release",
	Short: "Create a Cloud Deploy Release",
	Long: `This command depends on "cloud-deploy auth" command to generate auth file.
	This prototype provides limited functionality to create a release for demo purpose.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		// using default ADC auth (need to make sure credential file exist)
		ctx := context.Background()
		var cdClient *deploy.CloudDeployClient
		var sClient *storage.Client
		var err error
		if credentialsFile != "" {
			fmt.Println("CD using user-provided credential")
			credentials := os.Getenv(credentialsFile)
			credentialsBytes := []byte(credentials)
			cdClient, err = deploy.NewCloudDeployClient(ctx, option.WithUserAgent("google-gitlab-components:create-cloud-deploy-release"), option.WithCredentialsJSON(credentialsBytes))
			if err != nil {
				return fmt.Errorf("NewClient: %w", err)
			}
			sClient, err = storage.NewClient(ctx, option.WithUserAgent("google-gitlab-components:create-cloud-deploy-release"), option.WithCredentialsJSON(credentialsBytes))
			if err != nil {
				return fmt.Errorf("NewClient: %w", err)
			}
		} else {
			fmt.Println("CD using WIF credential")
			cdClient, err = deploy.NewCloudDeployClient(ctx, option.WithUserAgent("google-gitlab-components:create-cloud-deploy-release"))
			if err != nil {
				return fmt.Errorf("NewClient: %w", err)
			}
			sClient, err = storage.NewClient(ctx, option.WithUserAgent("google-gitlab-components:create-cloud-deploy-release"))
			if err != nil {
				return fmt.Errorf("NewClient: %w", err)
			}
		}

		defer cdClient.Close()
		defer sClient.Close()

		// TODO: validation
		// pull delivery pipeline, checking if it is suspended
		uuid, err := release.ValidateReleasePipeline(ctx, cdClient, projectId, region, deliveryPipeline)
		if err != nil {
			return err
		}
		// TODO: validate source & skaffold_file (for now, just support local path source: web/), so skipping

		// create default gcs bucket, and prepare release config
		req, err := release.CreateReleaseConfig(ctx, sClient, uuid, projectId, region, source, deliveryPipeline, releaseName)
		if err != nil {
			return err
		}

		op, err := cdClient.CreateRelease(ctx, req)
		if err != nil {
			return err
		}

		_, err = op.Wait(ctx)
		if err != nil {
			return err
		}

		// Create a Rollout
		if err = rollout.CreateRollout(ctx, cdClient, projectId, region, deliveryPipeline, releaseName); err != nil {
			return err
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(releaseCmd)
	releaseCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	releaseCmd.PersistentFlags().StringVarP(&releaseName, "release", "r", "", "Release Name")
	releaseCmd.PersistentFlags().StringVarP(&deliveryPipeline, "delivery-pipeline", "d", "", "Delivery Pipeline")
	releaseCmd.PersistentFlags().StringVarP(&source, "source", "", "", "Source")
	releaseCmd.PersistentFlags().StringVarP(&region, "region", "", "us-central1", "GCP Region")
	releaseCmd.PersistentFlags().StringVarP(&projectId, "project-id", "", "", "GCP Project ID")
	releaseCmd.PersistentFlags().StringVarP(&credentialsFile, "credentials_file", "", "", "User-provided credential file")
}
