# Cloud Deploy

This is a customized go binary based solution which is built on [Cloud Client Library](https://cloud.google.com/apis/docs/cloud-client-libraries).

The component:
  - Use `cloud-deploy auth` command to generate an crendential file to authenticate the cloud clients
  - Setup [GOOGLE_APPLICATION_CREDENTIALS](https://cloud.google.com/docs/authentication/application-default-credentials#GAC) for ADC auth
  - Use `cloud-deploy release` command (based on [cloud client libraries]((https://cloud.google.com/go/docs/reference/cloud.google.com/go/deploy/latest/apiv1))) to create a release.

Example usage can be found at: https://gitlab.com/quanzhang/my-component/-/blob/main/templates/cloud-deploy-img.yml?ref_type=heads