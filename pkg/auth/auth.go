package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Format struct {
	Type string `json:"type"`
}

type CredentialSource struct {
	File   string `json:"file"`
	Format Format `json:"format"`
}

type ServiceAccountImpersonation struct {
	TokenLifetimeSeconds int `json:"token_lifetime_seconds"`
}

type ExternalAccountConfig struct {
	Type                           string           `json:"type"`
	Audience                       string           `json:"audience"`
	SubjectTokenType               string           `json:"subject_token_type"`
	TokenURL                       string           `json:"token_url"`
	CredentialSource               CredentialSource `json:"credential_source"`
	ServiceAccountImpersonationURL string           `json:"service_account_impersonation_url"`
	//ServiceAccountImpersonation    ServiceAccountImpersonation `json:"service_account_impersonation"`
}

func CreateJWTFile(ciJobId, oidc string) (string, error) {
	path := fmt.Sprintf("/tmp/oidc-jwt-%s.txt", ciJobId)
	// Create file
	file, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println("Error closing file:", err)
		}
	}()

	// Write data to file
	data := []byte(oidc)
	_, err = file.Write(data)
	if err != nil {
		return "", err
	}

	return path, err
}

func CreateCredentialFile(ciId, jwtFilePath, wif, sa string) (string, error) {
	config := ExternalAccountConfig{
		Type:             "external_account",
		Audience:         wif,
		SubjectTokenType: "urn:ietf:params:oauth:token-type:jwt",
		TokenURL:         "https://sts.googleapis.com/v1/token",
		CredentialSource: CredentialSource{
			File: jwtFilePath,
			Format: Format{
				Type: "text",
			},
		},
		ServiceAccountImpersonationURL: fmt.Sprintf("https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/%s:generateAccessToken", sa),
		// ServiceAccountImpersonation: ServiceAccountImpersonation{
		// 	TokenLifetimeSeconds: 1800,
		// },
	}

	// Convert the struct to JSON
	jsonBytes, err := json.Marshal(config)
	if err != nil {
		return "", nil
	}
	// Write JSON data to file
	path := fmt.Sprintf("/tmp/oidc-credential-%s.json", ciId)
	err = ioutil.WriteFile(path, jsonBytes, 0644)
	if err != nil {
		return "", nil
	}

	return path, nil
}
