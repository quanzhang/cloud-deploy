package rollout

import (
	"context"
	"fmt"
	"math/rand"

	deploy "cloud.google.com/go/deploy/apiv1"
	"cloud.google.com/go/deploy/apiv1/deploypb"
	"github.com/google/uuid"
)

func CreateRollout(ctx context.Context, cdc *deploy.CloudDeployClient, projectId, region, pipeline, releaseName string) error {
	// create rollout
	// first get the created release
	releaseGetReq := &deploypb.GetReleaseRequest{
		Name: fmt.Sprintf("projects/%s/locations/%s/deliveryPipelines/%s/releases/%s", projectId, region, pipeline, releaseName),
	}
	resp, err := cdc.GetRelease(ctx, releaseGetReq)
	if err != nil {
		return err
	}

	// for now, we will just promote to the first target...
	// TODO: calculate the next stage to deploy
	stages := resp.DeliveryPipelineSnapshot.GetSerialPipeline().Stages
	toTarget := stages[0].TargetId
	//releaseId := resp.Uid
	rollOutId := generateRolloutId(releaseName, toTarget)
	rollout := &deploypb.Rollout{
		Name:     fmt.Sprintf("projects/%s/locations/%s/deliveryPipelines/%s/releases/%s/rollouts/%s", projectId, region, pipeline, releaseName, rollOutId),
		TargetId: toTarget,
	}

	req := &deploypb.CreateRolloutRequest{
		Parent:    fmt.Sprintf("projects/%s/locations/%s/deliveryPipelines/%s/releases/%s", projectId, region, pipeline, releaseName),
		RolloutId: rollOutId,
		RequestId: uuid.NewString(),
		Rollout:   rollout,
	}
	op, err := cdc.CreateRollout(ctx, req)
	if err != nil {
		return err
	}

	createResp, err := op.Wait(ctx)
	if err != nil {
		return err
	}
	// TODO: Use resp.
	_ = createResp

	// TODO: check if needs approval

	return nil
}

func generateRolloutId(releaseName, targetId string) string {
	return fmt.Sprintf("%s-to-%s-quan-test-%v", releaseName, targetId, rand.Intn(1000))
}
