package release

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	deploy "cloud.google.com/go/deploy/apiv1"
	"cloud.google.com/go/deploy/apiv1/deploypb"
	"cloud.google.com/go/storage"
	"github.com/google/uuid"
)

func ValidateReleasePipeline(ctx context.Context, cdClient *deploy.CloudDeployClient, projectId, region, pipeline string) (string, error) {
	req := &deploypb.GetDeliveryPipelineRequest{
		Name: fmt.Sprintf("projects/%s/locations/%s/deliveryPipelines/%s", projectId, region, pipeline),
	}
	resp, err := cdClient.GetDeliveryPipeline(ctx, req)
	if err != nil {
		return "", err
	}

	fmt.Printf("resolved pipeline: %s", resp.Name)
	return resp.Uid, nil
}

func CreateReleaseConfig(ctx context.Context, sClient *storage.Client, pipelineUUID, projectId, region, source, pipelineName, releaseName string) (*deploypb.CreateReleaseRequest, error) {
	// TODO: validate skaffold file exist

	// Create the default GCS bucket
	// TODO: deal with source start with "gs://"

	// create Tarball and upload to GCS bucket
	bucket, obj, err := createGCSBucketIfNotExist(ctx, sClient, pipelineUUID, projectId, source)
	if err != nil {
		return nil, err
	}

	// set some fields in release config
	skaffoldConfigUri := fmt.Sprintf("gs://%s/%s", bucket, obj)

	release := &deploypb.Release{
		Name:        fmt.Sprintf("projects/%s/locations/%s/deliveryPipelines/%s/releases/%s", projectId, region, pipelineName, releaseName),
		Description: "go binary release",
		// TODO: parse the hardcoded values
		BuildArtifacts: []*deploypb.BuildArtifact{
			{
				Image: "leeroy-web",
				Tag:   "us-central1-docker.pkg.dev/gitlab-zhangquan/web-app/leeroy-web:v1",
			}, {
				Image: "leeroy-app",
				Tag:   "us-central1-docker.pkg.dev/gitlab-zhangquan/web-app/leeroy-app:v1",
			},
		},
		SkaffoldConfigUri: skaffoldConfigUri,
	}

	req := &deploypb.CreateReleaseRequest{
		Parent:    fmt.Sprintf("projects/%s/locations/%s/deliveryPipelines/%s", projectId, region, pipelineName),
		ReleaseId: releaseName,
		Release:   release,
		RequestId: uuid.NewString(),
	}

	return req, nil
}

func createGCSBucketIfNotExist(ctx context.Context, sClient *storage.Client, pipelineUUID, projectId, source string) (string, string, error) {
	// getDefaultBucketName
	bucketName, err := getDefaultBucketName(pipelineUUID)
	if err != nil {
		return "", "", err
	}

	// TODO: add extra parsing
	// gcsSourceStagingDir := fmt.Sprintf("gs://%s/source", bucketName)
	// stage source to cloud storage
	stagedObj := fmt.Sprintf("%v-%s%s", time.Now().Unix(), uuid.New(), ".tgz")

	// TODO: use the get and then push mechanism, we just create bucket for now
	bucket := sClient.Bucket(bucketName)
	if err := bucket.Create(ctx, projectId, nil); err != nil {
		fmt.Errorf("failed to create bucket, %s", err)
	}

	// enable uniform level access
	enableUniformBucketLevelAccess := storage.BucketAttrsToUpdate{
		UniformBucketLevelAccess: &storage.UniformBucketLevelAccess{
			Enabled: true,
		},
	}
	if _, err := bucket.Update(ctx, enableUniformBucketLevelAccess); err != nil {
		return "", "", fmt.Errorf("Bucket(%q).Update: %w", bucketName, err)
	}

	// TODO: check access

	// Prepare tarball if needed
	fileToUpload := source
	fileInfo, err := os.Stat(source)
	if err != nil {
		return "", "", err
	}
	if fileInfo.Mode().IsDir() || !(strings.HasSuffix(source, ".tar.gz") && strings.HasSuffix(source, ".tar")) {
		// create the the tarball
		fileToUpload = source + ".tar.gz"
		if err := createTarball(source, fileToUpload); err != nil {
			return "", "", err
		}
	}

	// Upload Tarball
	stagedObj = "source/" + stagedObj
	data, err := ioutil.ReadFile(fileToUpload)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return "", "", fmt.Errorf("os.Open: %w", err)
	}

	o := sClient.Bucket(bucketName).Object(stagedObj)
	wc := o.NewWriter(ctx)
	wc.ContentType = "application/x-tar"

	// Write the tarball data to the object
	if _, err := wc.Write(data); err != nil {
		fmt.Printf("Error: %v\n", err)
		return "", "", fmt.Errorf("wc.write: %w", err)
	}

	// Close the object writer
	if err := wc.Close(); err != nil {
		fmt.Printf("Error: %v\n", err)
		return "", "", fmt.Errorf("wc.close: %w", err)
	}

	return bucketName, stagedObj, nil
}

func getDefaultBucketName(pipelineUUID string) (string, error) {
	bucketName := pipelineUUID + "_clouddeploy"
	if len(bucketName) > 63 {
		return "", fmt.Errorf("The length of the bucket id: {} must not exceed 63 characters")
	}

	return bucketName, nil
}

func CreateTarball(tarballFilePath string, filePaths []string) error {
	file, err := os.Create(tarballFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not create tarball file '%s', got error '%s'", tarballFilePath, err.Error()))
	}
	defer file.Close()

	gzipWriter := gzip.NewWriter(file)
	defer gzipWriter.Close()

	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	for _, filePath := range filePaths {
		err := addFileToTarWriter(filePath, tarWriter)
		if err != nil {
			return errors.New(fmt.Sprintf("Could not add file '%s', to tarball, got error '%s'", filePath, err.Error()))
		}
	}

	return nil
}

func addFileToTarWriter(filePath string, tarWriter *tar.Writer) error {
	file, err := os.Open(filePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not open file '%s', got error '%s'", filePath, err.Error()))
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return errors.New(fmt.Sprintf("Could not get stat for file '%s', got error '%s'", filePath, err.Error()))
	}

	header := &tar.Header{
		Name:    filePath,
		Size:    stat.Size(),
		Mode:    int64(stat.Mode()),
		ModTime: stat.ModTime(),
	}

	err = tarWriter.WriteHeader(header)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not write header for file '%s', got error '%s'", filePath, err.Error()))
	}

	_, err = io.Copy(tarWriter, file)
	if err != nil {
		return errors.New(fmt.Sprintf("Could not copy the file '%s' data to the tarball, got error '%s'", filePath, err.Error()))
	}

	return nil
}

func createTarball(folderToTarball, tarballPath string) error {
	// Create a new tarball file
	tarballFile, err := os.Create(tarballPath)
	if err != nil {
		return err
	}
	defer tarballFile.Close()

	// Create a gzip writer
	gzipWriter := gzip.NewWriter(tarballFile)
	defer gzipWriter.Close()

	// Create a tar writer
	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	err = filepath.Walk(folderToTarball, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := tar.FileInfoHeader(info, "")
		if err != nil {
			return err
		}
		relPath, err := filepath.Rel(folderToTarball, path)
		if err != nil {
			return err
		}
		header.Name = relPath
		// Write the header to the tarball
		if err := tarWriter.WriteHeader(header); err != nil {
			return err
		}

		// header.Name = filepath.Join(folderToTarball, path)
		if info.IsDir() {
			header.Mode |= 0755 // Set directory permission
		} else {
			data, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			header.Size = int64(len(data))
			tarWriter.WriteHeader(header)
			_, err = tarWriter.Write(data)
			if err != nil {
				return err
			}
		}
		return nil
	})

	return err
}
